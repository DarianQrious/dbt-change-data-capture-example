{% macro ods_store(source_table, pk, hash) %}
    select 
        a.*,
        1 AS SYS_VERSION,
        'Y' AS SYS_CURRENT_FLAG, 
        '1900-01-01' AS SYS_START_DATE, 
        '2999-12-31' AS SYS_END_DATE,
        {{dbt_utils.current_timestamp()}} AS SYS_CREATE_TIME,
        {{dbt_utils.current_timestamp()}} AS SYS_UPDATE_TIME
    from {{source_table}} a 
    left outer join {{this}} b on a.{{pk}} = b.{{pk}}
    where b.{{pk}} is null

    union all

    select
        a.*,
        SYS_VERSION + 1 AS SYS_VERSION,
        'Y' AS SYS_CURRENT_FLAG,
        {{ dbt_utils.current_timestamp() }} AS SYS_START_DATE,
        '2999-12-31'::timestamp AS SYS_END_DATE,
        {{ dbt_utils.current_timestamp() }} AS SYS_CREATE_TIME,
        {{ dbt_utils.current_timestamp() }} AS SYS_UPDATE_TIME
    FROM    {{source_table}} a INNER JOIN {{this}} b 
    ON      b.{{pk}} = a.{{pk}}
    WHERE   b.SYS_CURRENT_FLAG = 'Y' 
    AND     a.{{hash}} <> b.{{hash}}

    union all

    SELECT {{ dbt_utils.star(from=this, relation_alias='b', except=["SYS_VERSION", 
                                                                    "SYS_CURRENT_FLAG", 
                                                                    "SYS_START_DATE", 
                                                                    "SYS_END_DATE", 
                                                                    "SYS_CREATE_TIME", 
                                                                    "SYS_UPDATE_TIME"]) }},
            b.SYS_VERSION AS SYS_VERSION,
            'N' AS SYS_CURRENT_FLAG,
            b.SYS_START_DATE AS SYS_START_DATE,
            DATEADD(DAYS, -1, CAST(CURRENT_TIMESTAMP AS TIMESTAMP)) AS SYS_END_DATE,
            b.SYS_CREATE_TIME AS SYS_CREATE_TIME,
            {{ dbt_utils.current_timestamp() }} AS SYS_UPDATE_TIME
    FROM    {{source_table}} a INNER JOIN
            {{this}} b ON b.{{pk}} = a.{{pk}}
    WHERE   b.SYS_CURRENT_FLAG = 'Y' 
    AND     a.{{hash}} <> b.{{hash}}

{% endmacro %}